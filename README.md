
## General Description
This is a Scala based implementation (by Arnold Lacko <l.arnold@live.com>) of **_The Game of The Goose_**,
as described in the original assignment file 'GooseGame.md'.

---

## Additional rules
In addition to the original rules described in the above mentioned file, the game follows the following set of **additional rules**:

* A **move** of a player from position **_X_** to position **_Y_**, where **_Y < X_** (in other words a _"backwards move"_) will:
    * **NOT** cause another player to move, regardless if **_Y_** is already occupied by another player and **_Prank_** is enabled.
    * **NOT** cause the player to jump, regardless if **_Y_** is **_The Bridge_**.
    * **NOT** cause the player to move again, regardless if **_Y_** is **_The Goose_**.
* If **_Prank_** is enabled, in case if **multiple players** **_(p1, p2, ..., pn)_** occupy a position **_Y_** (which can happen as a result of players _bouncing_), the moving of a player from postition **_X_** to **_Y_** (assuming **_X < Y_**) will cause **all** players **_p1, p2, ..., pn_** to move back to position **_X_**.
 
---

## Main Class:
__arnold.goose.GooseGameConsole__