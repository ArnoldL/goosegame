package arnold.goose

import arnold.goose.board.Board
import arnold.goose.board.field._
import arnold.goose.command.{AddPlayer, Command, Move}
import arnold.goose.event._
import arnold.goose.exception.InvalidInputException

/**
  * The main class encapsulating the logic of the game.
  *
  * @param prankEnabled whether knock-back events should happen or not
  * @param board the board on which the game is played
  */
class GooseGame(val prankEnabled: Boolean)(implicit val board: Board) {

  /**
    * The state of the game which is being altered as a consequence of every command.
    */
  var gameState = GameState(Map())

  /**
    * Determines whether the game is over.
    *
    * @return whether the game is over
    */
  def isOver: Boolean = gameState.isOver

  /**
    * It processes a command and all its effects on the game.
    * <p>
    * It alters the game state as the effect of a command.
    *
    * @param command the command to process
    * @return the overall description of all events that happened as an effect of the command
    */
  def processCommand(command: Command): String = {
    val events = generateEvents(command)
    val results: List[(GameState, String)] = executeEvents(events, gameState)
    gameState = results.last._1
    results.map(_._2).mkString(" ")
  }

  /**
    * Based on the command's desired effect on the game, it generates an event and all its consequences,
    * in the form of an event list.
    *
    * @param command the command for which the resulting events are generated
    * @return an event and all its consequences in the form of an event list (in chronological order)
    */
  private[this] def generateEvents(command: Command): List[Event] = {
    command match {
      case AddPlayer(player) => List(processNewPlayer(player))
      case Move(player, roll) => processRoll(player, roll);
    }
  }

  /**
    * Handles the intent of adding a new player to this game, building the corresponding event.
    *
    * @param player the player that is being added to this game
    * @return the event representing the addition of a new player to this game
    */
  private[this] def processNewPlayer(player: Player): Event =
    if (gameState.players.contains(player))
      throw new InvalidInputException(s"$player: already existing player")
    else
      NewPlayerEvent(player)

  /**
    * Processes a roll of the dice by a player by generating all the consequent events.
    *
    * @param player the player that rolled the dice
    * @param roll the resulting dice-roll
    * @return the consequent events of the dice-roll
    */
  private[this] def processRoll(player: Player, roll: DiceRoll): List[Event] = {
    if (!gameState.players.contains(player))
      throw new InvalidInputException(s"Player $player does not exist")
    val rollSum = roll.sum
    RollEvent(player, roll) ::
      processMove(player, gameState.playerPositions(player),
        gameState.playerPositions(player) + rollSum, buildSimpleMoveEvent)
  }

  /**
    * Retrieves the list of players occupying a given position on the board.
    *
    * @param targetPos the ordinal number of the position on the board
    * @return the players occupying the given position (if there is one)
    */
  private[this] def playersOnPos(targetPos: Int): List[Player] =
    gameState.playerPositions.filter { case (_, pos: Int) => pos == targetPos }.keys.toList

  /**
    * Build the events representing the knocking back a player as a consequence of a 'Prank'.
    *
    * @param players the list of players being knocked back
    * @param fromPos the position the players will move back to
    * @return the events representing the players being knocked back to a position
    */
  private[this] def buildKnockBackEvents(players: List[Player], fromPos: Int): List[KnockBackEvent] =
    players.map(new KnockBackEvent(_, fromPos))

  /**
    * Processes a move as a consequence of a dice-roll or a previous move event
    * by generating the corresponding move event and all its consequences.
    *
    * @param player the player moving
    * @param fromPos the position the player is moving from
    * @param toPos the position the player is moving to
    * @param buildMoveEvent the function used to build the next move event,
    *                       since the type of move event depends on the previous
    *                       event of which this move is a consequence of
    * @return an event and all its consequences in the form of an event list (in chronological order)
    */
  private[this] def processMove(player: Player, fromPos: Int, toPos: Int, buildMoveEvent: BuildMove): List[Event] = {

    def consequence: List[Event] =
      board.field(toPos) match {
        case Goal(_) => List(WinEvent(player))
        case SimpleField(_) => List()
        case Bridge(_, bridgeTo) =>
          processMove(player, toPos, bridgeTo, buildJumpEvent)
        case Goose(_) =>
          processMove(player, toPos, toPos + (toPos - fromPos), buildGooseMoveEvent)
        case Bounce(_) if board.lastPosition == fromPos + 1 => List(WinEvent(player))
        case Bounce(_) =>
          List(new BounceEvent(player, fromPos + 1))
      }

    val knockedBackPlayers = if (prankEnabled) playersOnPos(toPos) else Nil
    buildMoveEvent(player, toPos) :: buildKnockBackEvents(knockedBackPlayers, fromPos) ::: consequence

  }

  /**
    * Retrieves the final game state and all the intermediary states
    * which result from the effects of a series of consecutive events,
    * along with the descriptions of each event.
    *
    * @param events the list of events being executed (in chronological order)
    * @param initialState the initial state unaffected by any of the events
    * @return a list of tuples where the first element of the n-th tuple is
    *         the game state after the effect of the n-th event (and all previous events),
    *         while the second element is the description of the n-th event
    *         with respect to the previous game state.
    */
  private[this] def executeEvents(events: List[Event], initialState: GameState): List[(GameState, String)] = {

    def executeEventsR(events: List[Event]): List[(GameState, String)] =
      events match {
        case head :: Nil => List((executeEvent(head, initialState), head.describe(initialState)))
        case head :: tail =>
          val rest = executeEventsR(tail)
          val lastState = rest.head._1
          (executeEvent(head, lastState), head.describe(lastState)) :: rest
      }

    executeEventsR(events.reverse).reverse

  }

  /**
    * Determines the effect of a particular event on the state of the game.
    *
    * @param event the event itself
    * @param gameState the game state in which the event occurs
    * @return the resulting game state affected by the event
    */
  private[this] def executeEvent(event: Event, gameState: GameState): GameState =
    event match {
      case NewPlayerEvent(player) => GameState(gameState.playerPositions + (player -> 0))
      case WinEvent(player) => GameState(gameState.playerPositions, Some(player))
      case MoveEvent(player, toPosition) => GameState(gameState.playerPositions + (player -> toPosition))
      case _ => gameState
    }

  /**
    * A trait representing a function for building a particular type of move event.
    */
  private trait BuildMove extends ((Player, Int) => MoveEvent)

  /**
    * A function building a [[arnold.goose.event.SimpleMoveEvent]].
    */
  private[this] val buildSimpleMoveEvent: BuildMove = (player, to) => new SimpleMoveEvent(player, to)

  /**
    * A function building a [[arnold.goose.event.JumpEvent]].
    */
  private[this] val buildJumpEvent: BuildMove = (player: Player, to: Int) => new JumpEvent(player, to)

  /**
    * A function building a [[arnold.goose.event.GooseMoveEvent]].
    */
  private[this] val buildGooseMoveEvent: BuildMove = (player: Player, to: Int) => new GooseMoveEvent(player, to)

}