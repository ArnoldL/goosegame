package arnold.goose

/**
  * A player of the game.
  *
  * @param name the name this player is identified by throughout the game
  */
case class Player(name: String) {

  override def toString: String = name

}
