package arnold.goose

import scala.util.Random

/**
  * The grouped representation of the result of rolling an arbitrary number of dice.
  *
  * @param dice a list of the numbers the individual die-rolls resulted in
  */
case class DiceRoll(dice: List[Int]) {

  /**
    * The sum of all individual die-rolls, representing the overall result of this roll.
    */
  val sum: Int = dice.sum

}

object DiceRoll {

  /**
    * Simulates an individual die-roll by automatically generating the resulting number.
    * <p>
    * Uses pseudo-random integers.
    *
    * @param dieSize the number of sides on the die
    * @return the resulting number after rolling the die
    */
  private def rollOneDie(dieSize: Integer): Int = new Random().nextInt(dieSize) + 1

  /**
    * Simulates rolling an arbitrary number of dice of a particular size.
    *
    * @param diceCount the number of dice
    * @param dieSize the size of each dice
    * @return an automatically generated dice-roll
    */
  def apply(diceCount: Integer, dieSize: Integer): DiceRoll =
    new DiceRoll((1 to diceCount).map(_ => rollOneDie(dieSize)).toList)

}
