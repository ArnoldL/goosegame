package arnold.goose

/**
  * Represents the state of the GooseGame at a given point in time.
  *
  * @param playerPositions the positions of each player on the board
  * @param winner the winner of the game
  */
case class GameState(playerPositions: Map[Player, Int], winner: Option[Player] = None) {

  /**
    * Determines if this state represents the game being over.
    *
    * @return whether this state represents the game being over.
    */
  def isOver: Boolean = winner.isDefined

  /**
    * Retrieves the set of players currently in the game
    *
    * @return the set of players currently in the game
    */
  def players: Set[Player] = playerPositions.keySet

}
