package arnold.goose.board.field

/**
  * A conceptual position which a bouncing player temporarily moves to before bouncing back.
  *
  * @param position the ordinal position where the bouncing player will temporarily move to before bouncing back
  */
case class Bounce(position: Int) extends Field {
  override def asString: String = position.toString
}
