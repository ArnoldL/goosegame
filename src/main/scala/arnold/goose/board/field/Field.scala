package arnold.goose.board.field

/**
  * A field on the GooseGame board.
  */
trait Field {

  /**
    * Introduced as a replacement of toString.
    *
    * @return a string representation of the field which is suited for the game's output
    */
  def asString: String

}
