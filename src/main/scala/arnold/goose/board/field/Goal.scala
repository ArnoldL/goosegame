package arnold.goose.board.field

/**
  * The goal, the winning position.
  *
  * @param position the ordinal position of the field on the board
  */
case class Goal(position: Int) extends Field {
  override def asString: String = position.toString
}
