package arnold.goose.board.field

/**
  * A regular field with no special treatment.
  *
  * @param position the ordinal position of the field on the board
  */
case class SimpleField(position: Int) extends Field {
  override def asString: String = position.toString
}
