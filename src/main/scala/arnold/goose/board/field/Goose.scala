package arnold.goose.board.field

/**
  * A field with the picture of the Goose on it.
  *
  * @param position the ordinal position of the field on the board
  */
case class Goose(position: Int) extends Field {
  override def asString: String = s"$position, The Goose"
}
