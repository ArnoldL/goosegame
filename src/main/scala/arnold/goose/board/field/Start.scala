package arnold.goose.board.field

/**
  * The starting field.
  */
object Start extends Field {
  override def asString: String = "Start"
}
