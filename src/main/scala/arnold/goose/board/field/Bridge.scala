package arnold.goose.board.field

/**
  * The Bridge.
  *
  * @param position the ordinal position of the field on the board.
  * @param to the ordinal position of the field that the bridge leads to
  */
case class Bridge(position: Int, to: Int) extends Field {
  override def asString: String = "The Bridge"
}
