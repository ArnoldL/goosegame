package arnold.goose.board

import arnold.goose.board.field._

/**
  * The board of the GooseGame.
  *
  * @param fields the fields constituting the board
  */
class Board(val fields: Array[Field]) {

  /**
    * The ordinal number of the last position on this board.
    */
  val lastPosition: Int = fields.length-1

  /**
    * Finds the field on the board at a given position.
    *
    * @param pos the ordinal position of the field on the board
    * @return the field on the board at a given position
    */
  def field(pos: Int): Field = if (pos < fields.length) fields(pos) else Bounce(lastPosition)

  override def toString: String =
    s"Board (${fields.indices.map(i => (i, fields(i))).toList.mkString("\n")})"

}

object Board {

  /**
    * Builds the board of the GooseGame.
    *
    * @param boardSize the number of fields on the board (not including the starting position)
    * @param geese the positions with a picture of the Goose on them
    * @param bridge the position and target position of the Bridge
    * @return a board of the GooseGame
    */
  def apply(boardSize: Int, geese: Set[Int], bridge: (Int, Int)) =
    new Board(
      (0 to boardSize).map {
        case 0 => Start
        case i if geese.contains(i) => Goose(i)
        case i if i == bridge._1 => Bridge(i, bridge._2)
        case i if i == boardSize => Goal(i)
        case i => SimpleField(i)
      }.toArray[Field]
    )

}