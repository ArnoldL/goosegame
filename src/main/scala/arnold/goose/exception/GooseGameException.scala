package arnold.goose.exception

/**
  * A checked exception within the GooseGame.
  *
  * @param message the description of this exception
  */
case class GooseGameException(message: String) extends Exception(message)
