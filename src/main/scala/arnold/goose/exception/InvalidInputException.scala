package arnold.goose.exception

/**
  * An exception representing an issue with the user input.
  *
  * @param message the description of this exception
  */
class InvalidInputException(message: String) extends GooseGameException(message)
