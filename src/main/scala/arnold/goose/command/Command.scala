package arnold.goose.command

/**
  * Represents a user command.
  */
trait Command
