package arnold.goose.command

import arnold.goose.Player

/**
  * The command for adding a new player to the game.
  *
  * @param player the player to add
  */
case class AddPlayer(player: Player) extends Command
