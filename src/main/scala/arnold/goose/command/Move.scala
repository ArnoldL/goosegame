package arnold.goose.command

import arnold.goose.{Player, DiceRoll}

/**
  * The command which initiates a roll and the moves of a player within the game.
  *
  * @param player the player to move
  * @param roll the roll of the dice
  */
case class Move(player: Player, roll: DiceRoll) extends Command
