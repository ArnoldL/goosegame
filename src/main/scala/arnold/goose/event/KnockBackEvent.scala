package arnold.goose.event

import arnold.goose.board.Board
import arnold.goose.{GameState, Player}

/**
  * The event of 'Prank', which is a player being knocked back by another player.
  *
  * @param player the player moving
  * @param toPosition the position the player is knocked back to
  */
class KnockBackEvent(player: Player, toPosition: Int) extends MoveEvent(player, toPosition) {

  override def describe(gameState: GameState)(implicit board: Board): String =
    s"On ${gameState.playerPositions(player)} there is $player, who returns to ${board.field(toPosition).asString}."

}
