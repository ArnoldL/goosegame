package arnold.goose.event

import arnold.goose.Player

/**
  * The event of a player moving on the board.
  *
  * @param player the player moving
  * @param toPosition the position the player is moving to
  */
abstract case class MoveEvent(player: Player, toPosition: Int) extends Event

