package arnold.goose.event

import arnold.goose.GameState
import arnold.goose.board.Board

/**
  * An event in the game, which may alter the state of the game either directly
  * or have the gameState changed indirectly by a consequent event.
  */
trait Event {

  /**
    * Forms a description of the event which may be presented to the user in a story-like manner.
    *
    * @param gameState The game state before being affected by this event
    * @param board The board on which the game is played
    * @return a description of the event
    */
  def describe(gameState: GameState)(implicit board: Board): String

}
