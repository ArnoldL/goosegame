package arnold.goose.event

import arnold.goose.board.Board
import arnold.goose.{GameState, Player}

/**
  * The event of a repeated move as a consequence of a move to a field with The Goose.
  *
  * @param player the player moving
  * @param toPosition the position the player is moving to
  */
class GooseMoveEvent(player: Player, toPosition: Int) extends MoveEvent(player, toPosition) {

  override def describe(gameState: GameState)(implicit board: Board): String =
    s"$player moves again and goes to ${board.field(toPosition).asString}."

}
