package arnold.goose.event

import arnold.goose.board.Board
import arnold.goose.{GameState, Player}

/**
  * The event of the game being won by a player.
  *
  * @param player the player winning the game
  */
case class WinEvent(player: Player) extends Event {

  override def describe(gameState: GameState)(implicit board: Board): String = s"$player wins!"

}
