package arnold.goose.event

import arnold.goose.board.Board
import arnold.goose.{GameState, Player, DiceRoll}

/**
  * The event of a player rolling the dice.
  *
  * @param player the player rolling the dice
  * @param roll the resulting dice-roll
  */
case class RollEvent(player: Player, roll: DiceRoll) extends Event {

  override def describe(gameState: GameState)(implicit board: Board): String =
    s"$player rolls ${roll.dice.mkString(", ")}."

}
