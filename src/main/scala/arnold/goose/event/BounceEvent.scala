package arnold.goose.event

import arnold.goose.board.Board
import arnold.goose.{GameState, Player}

/**
  * The event of a player bouncing back as a consequence of missing the goal.
  *
  * @param player the player moving
  * @param toPosition the position the player is moving back to
  */
class BounceEvent(player: Player, toPosition: Int) extends MoveEvent(player, toPosition) {

  override def describe(gameState: GameState)(implicit board: Board): String =
    s"$player bounces! $player returns to $toPosition"

}
