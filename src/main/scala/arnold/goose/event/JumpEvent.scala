package arnold.goose.event

import arnold.goose.board.Board
import arnold.goose.{GameState, Player}

/**
  * The event of a jump-move as a consequence of a move to The Bridge.
  *
  * @param player the player jumping
  * @param toPosition the position the player is jumping to
  */
class JumpEvent(player: Player, toPosition: Int) extends MoveEvent(player, toPosition) {

  override def describe(gameState: GameState)(implicit board: Board): String =
    s"$player jumps to ${board.field(toPosition).asString}."

}
