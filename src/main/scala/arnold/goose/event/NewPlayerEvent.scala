package arnold.goose.event

import arnold.goose.board.Board
import arnold.goose.{GameState, Player}

/**
  * The event of adding a new player to the game.
  *
  * @param player the player being added
  */
case class NewPlayerEvent(player: Player) extends Event {

  override def describe(gameState: GameState)(implicit board: Board): String =
    s"Players: ${(gameState.playerPositions.keySet + player).map(_.name).mkString(", ")}."

}


