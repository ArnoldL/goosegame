package arnold.goose.event

import arnold.goose.board.Board
import arnold.goose.{GameState, Player}

/**
  * The event of a simple move of a player on the board.
  *
  * @param player the player moving
  * @param toPosition the ordinal position the player is moving to
  */
class SimpleMoveEvent(player: Player, toPosition: Int) extends MoveEvent(player, toPosition) {

  override def describe(gameState: GameState)(implicit board: Board): String =
    s"$player moves from ${board.field(gameState.playerPositions(player)).asString}" +
      s" to ${board.field(toPosition).asString}."

}
