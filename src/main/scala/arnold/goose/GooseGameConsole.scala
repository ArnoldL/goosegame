package arnold.goose

import arnold.goose.board.Board
import arnold.goose.command.{AddPlayer, Command, Move}
import arnold.goose.exception.{GooseGameException, InvalidInputException}

import scala.io.StdIn
import scala.util.matching.Regex

object GooseGameConsole {

  /**
    * The number of fields on the board.
    */
  private val BOARD_SIZE = 63

  /**
    * The ordinal numbers of the fields with the picture of The Goose on them.
    */
  private val GEESE = Set(5, 9, 14, 18, 23, 27)

  /**
    * The ordinal numbers of the fields forming The Bridge.
    */
  private val BRIDGE_FROM_TO: (Int, Int) = 6 -> 12

  /**
    * The number of dice in play.
    */
  private val DICE_COUNT = 2

  /**
    * The number of sides of all dice.
    */
  private val DICE_SIZE = 6

  /**
    * The delimiter in the list of numbers forming a dice-roll.
    */
  private val ROLL_DELIMITER = ","

  /**
    * Defines the format of the command for
    * adding a new player to the game.
    */
  private val addPlayerCommandInput: Regex = "add player ([a-zA-Z][a-zA-Z0-9]*)".r

  /**
    * Defines the format of the command for
    * moving a player based on a specified dice-roll.
    */
  private val moveCommandInput: Regex = s"move ([a-zA-Z][a-zA-Z0-9]*) ([0-9]+(?:$ROLL_DELIMITER [0-9]+)*)".r

  /**
    * Defines the format of the command for moving a
    * player based on an automatically generated dice-roll.
    */
  private val autoRollCommandInput: Regex = "move ([a-zA-Z][a-zA-Z0-9]*)".r

  /**
    * Defines whether 'Prank' is enabled in the game.
    * <p>
    * When 'Prank' is enabled, if player X moves to a position
    * occupied by player Y, player Y will move back to player X's
    * previous position on the board.
    */
  private implicit val PRANK_ENABLED: Boolean = true

  /**
    * The board being constructed outside of the game, to make the game independent of the layout of the board.
    * <p>
    * In a future version the board layout may be read from an external file set by the user.
    */
  private implicit val board: Board = Board(BOARD_SIZE, GEESE, BRIDGE_FROM_TO)

  /**
    * The game being constructed based on the board and based on whether 'prank' is enabled.
    */
  private val game = new GooseGame(PRANK_ENABLED)

  /**
    * The main entry point to the game's command line based UI.
    * <p>
    * The method contains the main game-loop, reading commands from the user,
    * parsing them, passing them to the game and printing the description of
    * the results.
    *
    * @param args the command line arguments
    */
  def main(args: Array[String]): Unit = {
    while (!game.isOver) {
      try {
        val command = parseCommand(StdIn.readLine())
        println(game.processCommand(command))
      }
      catch {
        case GooseGameException(m) => println(m)
      }
    }
  }

  /**
    * Parses a dice-roll input.
    *
    * @param input the dice-roll input to parse
    * @return the resulting dice-roll
    */
  def parseRoll(input: String): DiceRoll = {
    val splitInput = input.split(ROLL_DELIMITER)
    if (splitInput.size != DICE_COUNT)
      throw new InvalidInputException(s"Number of dice should be $DICE_COUNT")
    val diceRolls = splitInput.map(_.trim).map(Integer.parseInt).toList
    if (diceRolls.exists(r => r > DICE_SIZE || r < 1))
      throw new InvalidInputException(s"Dice rolls must be between 1 and $DICE_COUNT")
    DiceRoll(diceRolls)
  }

  /**
    * Parses an input command.
    *
    * @param input the input to parse
    * @return the resulting command
    */
  def parseCommand(input: String): Command =
    input match {
      case addPlayerCommandInput(playerName) => AddPlayer(Player(playerName))
      case moveCommandInput(playerName, roll) => Move(Player(playerName), parseRoll(roll))
      case autoRollCommandInput(playerName) => Move(Player(playerName), DiceRoll(DICE_COUNT, DICE_SIZE))
      case _ => throw new InvalidInputException("Unrecognized command")
    }

}
